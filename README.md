# Programowanie genetyczne

## TinyGP-Java
Co jest w folderach:
* import - pełne wyjścia z pewnych uruchomień TinyGp
* zad - dane wejściowe do TinyGp (fitnesscases) i dwa skrypty, z których każdy generuje te pliki (jeden w C++, drugi w Pythonie)
* rozw - zapisane rozwiązania problemów
* src:
    * getresult.py - wyciąga ze standardowego wejścia formułę nastepującą po ostatnim wystąpieniu 'Best Individual' i zapisuje do pliku podanego jako argument
    * start.sh skrót do uruchomienia TinyGp na danym przykładzie i zapisania wyniku w ../rozw przez getresult.py
    * **tiny_gp.java**
    * uruchamianie.txt - przydatne polecenia

## Upraszczacz - upraszczanie wyrażeń algebraicznych
Co jest w folderach:
Pliki wynikowe znajdują się w :
./Upraszczacz/out/artifacts/Upraszczacz_jar
(To jest projekt Intellij Idea)
* out
    * /artifacts/Upraszczacz_jar/ - **KATALOG Z WYNIKAMI**
        * Upraszczacz.jar - jar aplikacji (wypisuje możliwe opcje gdy sie wywoła java -jar --help)
        * excelmake.py - skrypt do wczytywania csv i generowania z nich arkuszy excela oraz wykresów za pomoca matplotlib (Potrafi też dokonać złączenia danych z wielu csv pod pewnymi warunkami).
        * zad - problemy w formacie wejściowym TinyGp
        * rozw - rozwiązania (formuły) wygenerowane przez TinyGp (przekopiowane z TinyGP-Java/rozw)
        * uproszczone_rozw - wyniki pracy Upraszczacz
        * wyniki_csv - wyniki ewaluacji formuł z rozw na zbiorach z zad przez Upraszczacz (który potrafi też ewaluować wczytane wyrażenia)
        * **wyniki_excel**
            * **arkusze wynikowe będące efektem pracy excelmake.py na zawartości wyniki_csv**
            * obrazki - wygenerowane przez excelmake.py obrazki z wykresami.
            * **dodatkowe** - wyniki dla zadań dodatkowych
* lib
    * antlr4
    * org.apache.commons.cli - dla parsowania opcji w argumentach programu
* src - kod źródłowy Upraszczacza

grammar logiczne;
import lekslogicz;
//program : wyrazenie EOF;
plik: linia? (NL* linia)* EOF;
linia : 'E' STALA 'd' STALA ':' wyrazenie;
wyrazenie
          : ZMIENNA STALA                           #wyrazenieRownosc//
          | ZMIENNA                                 #wyrazenieZmienna//
          | znak=('-'| '+') wyrazenie                   #wyrazenieZnak//
          | '!' wyrazenie                           #wyrazenieNegacja//
          | wyrazenie mult=('*' | '/' |'%') wyrazenie   #wyrazenieMult//
          | wyrazenie addyt=('+' | '-') wyrazenie       #wyrazenieAddyt//
          | wyrazenie logicz=('^' | 'v')wyrazenie    #wyrazenieLogicz
          | STALA                             #wyrazenieStala//
          | '(' wyrazenie ')'                           #wyrazenieNawiasy//
          ;
wywolanie_funkcji   :  NAZWA '(' lista_parametrow_aktualnych ')';
lista_parametrow_aktualnych : (wyrazenie  (',' wyrazenie)*)?;
//zmienna: ZMIENNA;
//stala_atom: STALA;
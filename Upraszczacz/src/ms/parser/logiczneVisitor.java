// Generated from java-escape by ANTLR 4.11.1
package ms.parser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link logiczneParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface logiczneVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link logiczneParser#plik}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlik(logiczneParser.PlikContext ctx);
	/**
	 * Visit a parse tree produced by {@link logiczneParser#linia}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLinia(logiczneParser.LiniaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code wyrazenieMult}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWyrazenieMult(logiczneParser.WyrazenieMultContext ctx);
	/**
	 * Visit a parse tree produced by the {@code wyrazenieNegacja}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWyrazenieNegacja(logiczneParser.WyrazenieNegacjaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code wyrazenieNawiasy}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWyrazenieNawiasy(logiczneParser.WyrazenieNawiasyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code wyrazenieZmienna}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWyrazenieZmienna(logiczneParser.WyrazenieZmiennaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code wyrazenieAddyt}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWyrazenieAddyt(logiczneParser.WyrazenieAddytContext ctx);
	/**
	 * Visit a parse tree produced by the {@code wyrazenieRownosc}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWyrazenieRownosc(logiczneParser.WyrazenieRownoscContext ctx);
	/**
	 * Visit a parse tree produced by the {@code wyrazenieStala}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWyrazenieStala(logiczneParser.WyrazenieStalaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code wyrazenieLogicz}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWyrazenieLogicz(logiczneParser.WyrazenieLogiczContext ctx);
	/**
	 * Visit a parse tree produced by the {@code wyrazenieZnak}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWyrazenieZnak(logiczneParser.WyrazenieZnakContext ctx);
	/**
	 * Visit a parse tree produced by {@link logiczneParser#wywolanie_funkcji}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWywolanie_funkcji(logiczneParser.Wywolanie_funkcjiContext ctx);
	/**
	 * Visit a parse tree produced by {@link logiczneParser#lista_parametrow_aktualnych}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLista_parametrow_aktualnych(logiczneParser.Lista_parametrow_aktualnychContext ctx);
}
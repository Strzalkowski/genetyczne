// Generated from java-escape by ANTLR 4.11.1
package ms.parser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link logiczneParser}.
 */
public interface logiczneListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link logiczneParser#plik}.
	 * @param ctx the parse tree
	 */
	void enterPlik(logiczneParser.PlikContext ctx);
	/**
	 * Exit a parse tree produced by {@link logiczneParser#plik}.
	 * @param ctx the parse tree
	 */
	void exitPlik(logiczneParser.PlikContext ctx);
	/**
	 * Enter a parse tree produced by {@link logiczneParser#linia}.
	 * @param ctx the parse tree
	 */
	void enterLinia(logiczneParser.LiniaContext ctx);
	/**
	 * Exit a parse tree produced by {@link logiczneParser#linia}.
	 * @param ctx the parse tree
	 */
	void exitLinia(logiczneParser.LiniaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code wyrazenieMult}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void enterWyrazenieMult(logiczneParser.WyrazenieMultContext ctx);
	/**
	 * Exit a parse tree produced by the {@code wyrazenieMult}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void exitWyrazenieMult(logiczneParser.WyrazenieMultContext ctx);
	/**
	 * Enter a parse tree produced by the {@code wyrazenieNegacja}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void enterWyrazenieNegacja(logiczneParser.WyrazenieNegacjaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code wyrazenieNegacja}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void exitWyrazenieNegacja(logiczneParser.WyrazenieNegacjaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code wyrazenieNawiasy}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void enterWyrazenieNawiasy(logiczneParser.WyrazenieNawiasyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code wyrazenieNawiasy}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void exitWyrazenieNawiasy(logiczneParser.WyrazenieNawiasyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code wyrazenieZmienna}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void enterWyrazenieZmienna(logiczneParser.WyrazenieZmiennaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code wyrazenieZmienna}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void exitWyrazenieZmienna(logiczneParser.WyrazenieZmiennaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code wyrazenieAddyt}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void enterWyrazenieAddyt(logiczneParser.WyrazenieAddytContext ctx);
	/**
	 * Exit a parse tree produced by the {@code wyrazenieAddyt}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void exitWyrazenieAddyt(logiczneParser.WyrazenieAddytContext ctx);
	/**
	 * Enter a parse tree produced by the {@code wyrazenieRownosc}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void enterWyrazenieRownosc(logiczneParser.WyrazenieRownoscContext ctx);
	/**
	 * Exit a parse tree produced by the {@code wyrazenieRownosc}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void exitWyrazenieRownosc(logiczneParser.WyrazenieRownoscContext ctx);
	/**
	 * Enter a parse tree produced by the {@code wyrazenieStala}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void enterWyrazenieStala(logiczneParser.WyrazenieStalaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code wyrazenieStala}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void exitWyrazenieStala(logiczneParser.WyrazenieStalaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code wyrazenieLogicz}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void enterWyrazenieLogicz(logiczneParser.WyrazenieLogiczContext ctx);
	/**
	 * Exit a parse tree produced by the {@code wyrazenieLogicz}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void exitWyrazenieLogicz(logiczneParser.WyrazenieLogiczContext ctx);
	/**
	 * Enter a parse tree produced by the {@code wyrazenieZnak}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void enterWyrazenieZnak(logiczneParser.WyrazenieZnakContext ctx);
	/**
	 * Exit a parse tree produced by the {@code wyrazenieZnak}
	 * labeled alternative in {@link logiczneParser#wyrazenie}.
	 * @param ctx the parse tree
	 */
	void exitWyrazenieZnak(logiczneParser.WyrazenieZnakContext ctx);
	/**
	 * Enter a parse tree produced by {@link logiczneParser#wywolanie_funkcji}.
	 * @param ctx the parse tree
	 */
	void enterWywolanie_funkcji(logiczneParser.Wywolanie_funkcjiContext ctx);
	/**
	 * Exit a parse tree produced by {@link logiczneParser#wywolanie_funkcji}.
	 * @param ctx the parse tree
	 */
	void exitWywolanie_funkcji(logiczneParser.Wywolanie_funkcjiContext ctx);
	/**
	 * Enter a parse tree produced by {@link logiczneParser#lista_parametrow_aktualnych}.
	 * @param ctx the parse tree
	 */
	void enterLista_parametrow_aktualnych(logiczneParser.Lista_parametrow_aktualnychContext ctx);
	/**
	 * Exit a parse tree produced by {@link logiczneParser#lista_parametrow_aktualnych}.
	 * @param ctx the parse tree
	 */
	void exitLista_parametrow_aktualnych(logiczneParser.Lista_parametrow_aktualnychContext ctx);
}
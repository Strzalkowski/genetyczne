package ms;

import ms.bledy.SyntaxErrorListener;
import ms.drzewo.Węzeł;
import ms.drzewo.operacje.KonstrukcjaDrzewa;
import ms.drzewo.operacje.KonstrukcjaDrzewaZRegul;
import ms.interpreter.Silnik;
import ms.parser.*;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import static java.lang.System.exit;

public class AlarmRules {
    protected Silnik eng = new Silnik();
    public String setRules(String rules_text)
    {
        SyntaxErrorListener syntaxErrorListener = new SyntaxErrorListener();
        //1.analiza leksykalna
        var in =  CharStreams.fromString(rules_text);
        logiczneLexer lexer = new logiczneLexer(in);
        lexer.addErrorListener(syntaxErrorListener);
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        //2.analiza syntaktyczna
        logiczneParser parser = new logiczneParser(tokens);

        parser.addErrorListener(syntaxErrorListener);
        ParseTree tree = parser.plik(); // parse; start at prog <label id="code.tour.main.6"/>
        if(syntaxErrorListener.getNumberOfErrors() > 0)
        {
            return syntaxErrorListener.toString();//"WYSTĄPIŁY BŁĘDY SKŁADNIOWE, dokładnie "+syntaxErrorListener.getNumberOfErrors()+". Dalsza analiza i generacja nie nastąpi.";
        }

        logiczneVisitor<Węzeł> konstruktorDrzewa = new KonstrukcjaDrzewaZRegul();
        this.eng.załaduj((Węzeł) konstruktorDrzewa.visit(tree));
        return null;
    }
    public int checkAlarm(int d,int w,int f,int t,int r,int a){return checkAlarm((double)d,w,f,t,r,a);}

    /**
     * @param d
     * @param w
     * @param f
     * @param t
     * @param r
     * @param a
     * @return Threat severuty (E)//number of first rule(line) which was true, starting with 0; -1 if none fired.
     */
    public int checkAlarm(double d,double w,double f,double t,double r,double a) {
        this.eng.ustaw_zmienną("E",0);
        this.eng.ustaw_zmienną("d",d);
        this.eng.ustaw_zmienną("w",w);
        this.eng.ustaw_zmienną("w",f);
        this.eng.ustaw_zmienną("f",t);
        this.eng.ustaw_zmienną("t",r);
        this.eng.ustaw_zmienną("r",r);
        this.eng.ustaw_zmienną("a",a);
        this.eng.ustaw_zmienną("rule_num",this.eng.wykonaj());
        return (int) this.eng.podaj_zmienną("E");
    }
    public int checkLastRule()
    {
        return (int) this.eng.podaj_zmienną("rule_num");
    }
}

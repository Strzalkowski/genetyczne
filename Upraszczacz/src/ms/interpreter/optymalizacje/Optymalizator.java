package ms.interpreter.optymalizacje;

import ms.Tablice;
import ms.drzewo.Węzeł;

import static java.lang.Math.abs;

public class Optymalizator {
    Węzeł drzewo=null;
    public Optymalizator(Węzeł drzewo)
    {
        this.drzewo=drzewo;
    }
    public void spłaszczanie()
    {

    }
    public void redukcje()
    {
        redukuj(drzewo);
    }
    private void pisz_redukcję(Węzeł rodzic, Węzeł dz1, Węzeł dz2)
    {
        System.out.print("RED:"+rodzic.txtRepr()+"|"+dz1.txtRepr()+"|"+dz2.txtRepr());
        System.out.println("||:"+rodzic.internalRepr()+"|"+dz1.internalRepr()+"|"+dz2.internalRepr());
    }
    private void redukuj(Węzeł r)
    {
        for (var dz:r.dzieci)
        {
            redukuj(dz);
        }
        if(r.dzieci.size() == 2)
        {
            Węzeł a = r.dzieci.get(0);
            Węzeł b = r.dzieci.get(1);
            if(a.zawartość == Węzeł.Zawartość.JEDNOMIAN && b.zawartość == Węzeł.Zawartość.JEDNOMIAN
                    &&!(a.zm!=null && b.zm!= null && !(a.zm.equals(b.zm))))//Wykluczamy dwie nie puste, różne zmienne
            {

                switch (r.zawartość)
                {
                    case PLUS:
                    case MINUS:
                        if((a.st == b.st) && ((a.zm == null)?(b.zm == null):(a.zm.equals(b.zm))))//&& ((a.zm == null)?(b.zm == null):(a.zm.equals(b.zm)))
                        {
                            r.st=a.st;

                            switch (r.zawartość)
                            {
                                case PLUS:r.wt = a.wt + b.wt;
                                break;
                                case MINUS:r.wt = a.wt - b.wt;
                                break;
                            }
                        }
                        else{return;}
                        break;

                    case RAZY:
                    case DZIEL:
                        switch (r.zawartość)
                        {
                            case RAZY:r.wt = a.wt * b.wt;
                                    r.st = a.st + b.st;
                                break;
                            case DZIEL:
                                if(b.zm == null && abs(b.wt)<= Tablice.PRÓG_DZIWNODZIELENIA)
                                {
                                    r.wt=a.wt;
                                    r.st=a.st;
                                }
                                else{
                                    r.wt = a.wt / b.wt;
                                    r.st = a.st - b.st;
                                }

                                break;
                            default:return;
                        }
                        break;
                    default:return;
                }
                //pisz_redukcję(r,a,b);
                //zmiana obecnego węzła
                r.zm = (a.zm==null)?(b.zm):(a.zm);
                r.zawartość = Węzeł.Zawartość.JEDNOMIAN;
                r.dzieci.clear();//odcinanie dzieci!

            }
        }
    }
}

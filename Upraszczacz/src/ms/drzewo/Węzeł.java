package ms.drzewo;

import org.antlr.v4.runtime.Token;

import java.util.LinkedList;
import java.util.List;

import static java.lang.Math.abs;

/*
Odpowiada węzłowi drzewa składniowego programu. W przeciwienstwie do drzewa produkowanego przez antlr, zakłada się tu mutowalmość tego konstruktu.
 */
public class Węzeł {
    public Węzeł rodzic=null;
    public List<Węzeł> dzieci=new LinkedList<>();
    public int ilosc_dzieci = 2;

    public Zawartość zawartość=Zawartość.JEDNOMIAN;
    public String nazwaFunkcji=null;
    //kodowanie jednomianu
    public String zm=null;//nazwa zmiennej
    public int st=0;//stopień jednomianu
    public double wt=.0;//wartość współczynnika przed zmienną

    public Token start=null;
    public Token stop=null;
    public Węzeł loc(Token start, Token stop){this.start=start;this.stop=stop;return this;}

    public Węzeł() {

    }
    public Węzeł(Zawartość z) {
        zawartość=z;
    }


    public Węzeł(String nazwaFunkcji) {
        this.nazwaFunkcji = nazwaFunkcji;
        zawartość=Zawartość.FUNKCJA;
    }

    public Węzeł(String zm, int st, double wt) {
        this.zm = zm;
        this.st = st;
        this.wt = wt;
        zawartość=Zawartość.JEDNOMIAN;
    }

    public String kodPodrzewa()
    {
        switch(zawartość){
            case FUNKCJA: {
                return nazwaFunkcji+ "( "+dzieci.get(0).kodPodrzewa()+" )";
            }
            case JEDNOMIAN:{
                if(zm==null || st==0)
                {
                    return ""+wt;
                }
                else{
                    var znak = (st>0)?("*"):("/");
                    String środek;
                    if(wt==1 && st>0){środek=""+zm;}
                    else{
                        środek = "("+wt+znak+zm+")";
                    }

                    return "(".repeat(abs(st)-1)+środek+(znak+zm+")").repeat(abs(st)-1);
                }
            }
            case PLUS,RAZY,DZIEL,MINUS,LUB,ORAZ,POROWNANIE:{
                return "( "+dzieci.get(0).kodPodrzewa()+" "+zawartość+" "+dzieci.get(1).kodPodrzewa()+" )";
            }
            default:{return "(???)";}
        }
    }
    public String txtRepr(){
        return switch (zawartość){
            case FUNKCJA -> this.nazwaFunkcji;
            case JEDNOMIAN -> ""+wt+((zm==null)?(""):(zm+((st==1)?(""):("^"+st))));
            default -> zawartość.toString();
        };
    }
    public String internalRepr(){return "["+zawartość+ "]("+zm+","+st+","+wt+")";}
    public String dotId(){return "N"+System.identityHashCode(this);}
    public String dotRepr()
    {
        StringBuilder sb = new StringBuilder();
        for (var dz:dzieci) {
            sb.append(this.dotId());
            sb.append(" -> ");
            sb.append(dz.dotId());
            sb.append(";\n");
        }
        sb.append(this.dotId());
        sb.append("[label=\"");
        sb.append(this.txtRepr());
        sb.append("\"];\n");
        return sb.toString();
    }

    public enum Zawartość{
        JEDNOMIAN(1), PLUS(2),MINUS(3),RAZY(4),DZIEL(5),FUNKCJA(6),
        LUB(7),LUB_LICZĄCE(8),ORAZ(9),POROWNANIE(10),NEGACJA(11), REGULA_LOGICZNA(12);
        public final Integer numerek;

        Zawartość(Integer numerek) {
            this.numerek = numerek;
        }
        public String toString()
        {
            return switch (numerek) {
                case 1 -> "JEDNOMIAN";
                case 2 -> "+";
                case 3 -> "-";
                case 4 -> "*";
                case 5 -> "/";
                case 6 -> "FUNKCJA";
                case 7 -> "||";
                case 8 -> "LUB_LICZĄCE";
                case 9 -> "&&";
                case 10 -> "==";
                case 11 -> "!";
                case 12 -> "REGULA_LOGICZNA";
                default -> "?";
            };
        }

        public static Zawartość zNapisu(String napis)
        {
            return switch (napis) {
                case "+", "PLUS" -> PLUS;
                case "-", "MINUS" -> MINUS;
                case "*", "RAZY" -> RAZY;
                case "/", "DZIEL" -> DZIEL;
                case "||","|","v", "LUB" -> LUB;
                case "&&","&","^", "ORAZ" -> ORAZ;
                case "fun", "FUNKCJA" -> FUNKCJA;
                case "JEDNOMIAN" -> JEDNOMIAN;
                case "==" -> POROWNANIE;
                case "!" -> NEGACJA;
                default -> throw new RuntimeException("Napisu \""+napis+"\" nie da się przekonwertować do typu Zawartość");
            };
        };
    }
}

package ms.drzewo.operacje;

import ms.drzewo.Węzeł;
import ms.parser.logiczneBaseVisitor;
import ms.parser.logiczneParser;
import ms.parser.wyrBaseVisitor;
import ms.parser.logiczneParser;

import static ms.drzewo.Węzeł.Zawartość.*;

public class KonstrukcjaDrzewaZRegul extends logiczneBaseVisitor<Węzeł> {


    @Override
    public Węzeł visitPlik(logiczneParser.PlikContext ctx) {
        Węzeł wz = new Węzeł();
        wz.zawartość = LUB_LICZĄCE;
        for (var dz:ctx.linia()) {
            Węzeł dzw = (Węzeł) visit(dz);
            dzw.rodzic = wz;
            wz.dzieci.add(dzw);
        }
        return wz;
    }

    @Override
    public Węzeł visitLinia(logiczneParser.LiniaContext ctx) {
        //       wz
        //      /   \
        //   E     oraz
        //       /       \
        //    d_porown   wyr
        //  /   \
        //d  d_oczekiw
        Węzeł wz = new Węzeł();
        wz.zawartość = REGULA_LOGICZNA;
        Węzeł E = new Węzeł(JEDNOMIAN);
        E.rodzic = wz;
        E.zm="E";
        E.st=1;
        E.wt = Double.parseDouble(ctx.STALA(0).getText());
        wz.dzieci.add(E);

        Węzeł oraz = new Węzeł(ORAZ);

        Węzeł d = new Węzeł(JEDNOMIAN);
        d.rodzic=wz;
        d.zm = "d";
        d.st=1;
        d.wt= 1;

        Węzeł d_oczekiw = new Węzeł(JEDNOMIAN);
        d_oczekiw.zm = null;
        d_oczekiw.st = 0;
        d_oczekiw.wt = Double.parseDouble(ctx.STALA(1).getText());

        Węzeł d_porown = new Węzeł(POROWNANIE);
        d.rodzic = oraz;
        d_oczekiw.rodzic = oraz;
        d_porown.dzieci.add(d);
        d_porown.dzieci.add(d_oczekiw);
        d_porown.rodzic = oraz;


        Węzeł wyr = (Węzeł) visit(ctx.wyrazenie());
        wyr.rodzic = oraz;

        oraz.dzieci.add(d_porown);
        oraz.dzieci.add(wyr);
        wz.dzieci.add(oraz);
        return wz;
    }

    @Override
    public Węzeł visitWyrazenieNawiasy(logiczneParser.WyrazenieNawiasyContext ctx) {
        return (Węzeł)visit(ctx.wyrazenie());
    }

    @Override
    public Węzeł visitWyrazenieZnak(logiczneParser.WyrazenieZnakContext ctx) {
        Węzeł w = visit(ctx.wyrazenie());
        //Jesli trafimy na minusa, mnożymy podrzewo przez -1.
        if(ctx.znak.getText().equals("-"))
        {
            Węzeł r = new Węzeł(Węzeł.Zawartość.RAZY);
            Węzeł z = new Węzeł(null,0,-1);
            r.dzieci.add(z);
            r.dzieci.add(w);
            return r;
        }
        else{return w;}
    }

/*    @Override
    public Węzeł visitWyrazenieWywolanie(logiczneParser.WyrazenieWywolanieContext ctx) {
        Węzeł wz = new Węzeł(ctx.wywolanie_funkcji().NAZWA().getText());
        for (var dz:ctx.wywolanie_funkcji().lista_parametrow_aktualnych().wyrazenie()) {
            Węzeł dzw = (Węzeł) visit(dz);
            dzw.rodzic = wz;
            wz.dzieci.add(dzw);
        }
        return wz;
    }
*/
    @Override
    public Węzeł visitWyrazenieAddyt(logiczneParser.WyrazenieAddytContext ctx) {

        Węzeł wz = new Węzeł(Węzeł.Zawartość.zNapisu(ctx.addyt.getText()));
        for (var dz:ctx.wyrazenie()) {
            Węzeł dzw = (Węzeł) visit(dz);
            dzw.rodzic = wz;
            wz.dzieci.add(dzw);
        }
        return wz;
    }

    @Override
    public Węzeł visitWyrazenieMult(logiczneParser.WyrazenieMultContext ctx) {
        Węzeł wz = new Węzeł(Węzeł.Zawartość.zNapisu(ctx.mult.getText()));
        for (var dz:ctx.wyrazenie()) {
            Węzeł dzw = visit(dz);
            dzw.rodzic = wz;
            wz.dzieci.add(dzw);
        }
        return wz;
    }
    public Węzeł visitWyrazenieLogicz(logiczneParser.WyrazenieLogiczContext ctx) {
        Węzeł wz = new Węzeł(Węzeł.Zawartość.zNapisu(ctx.logicz.getText()));
        for (var dz:ctx.wyrazenie()) {
            Węzeł dzw = visit(dz);
            dzw.rodzic = wz;
            wz.dzieci.add(dzw);
        }
        return wz;
    }

    @Override
    public Węzeł visitWyrazenieNegacja(logiczneParser.WyrazenieNegacjaContext ctx) {
        Węzeł w = visit(ctx.wyrazenie());
        //Jesli trafimy na minusa, mnożymy podrzewo przez -1.
        //if(ctx.znak.getText().equals("-"))
        {
            Węzeł r = new Węzeł(Węzeł.Zawartość.RAZY);
            Węzeł z = new Węzeł(null,0,-1);
            r.dzieci.add(z);
            r.dzieci.add(w);
            return r;
        }
        //else{return w;}
    }

    @Override
    public Węzeł visitWyrazenieRownosc(logiczneParser.WyrazenieRownoscContext ctx) {
        Węzeł wz = new Węzeł(POROWNANIE);
        Węzeł zm = new Węzeł(null,0,Double.parseDouble(ctx.STALA().getText())).loc(ctx.start,ctx.stop);
        Węzeł st = new Węzeł(ctx.ZMIENNA().getText(),1,1.0).loc(ctx.start,ctx.stop);
        zm.rodzic=wz;st.rodzic=wz;
        wz.dzieci.add(zm);wz.dzieci.add(st);
        return wz;
    }

    @Override
    public Węzeł visitWyrazenieStala(logiczneParser.WyrazenieStalaContext ctx) {
        return new Węzeł(null,0,Double.parseDouble(ctx.STALA().getText())).loc(ctx.start,ctx.stop);
    }

    @Override
    public Węzeł visitWyrazenieZmienna(logiczneParser.WyrazenieZmiennaContext ctx) {
        return new Węzeł(ctx.ZMIENNA().getText(),1,1.0).loc(ctx.start,ctx.stop);
    }
}

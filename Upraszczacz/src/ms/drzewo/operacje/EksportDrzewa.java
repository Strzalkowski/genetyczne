package ms.drzewo.operacje;

import ms.drzewo.Węzeł;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class EksportDrzewa {

    static String fname="src/pyton/in.dot";
    private static int num=0;
    private static int liczbawezlow=0;

    public static void graphhviz(Węzeł korzeń)
    {
        liczbawezlow=0;
        num++;
        try {
            File kod = new File(fname+num);
            if (!kod.exists()) {
                //System.out.println(fname+num);
                kod.createNewFile();
            }
            FileWriter fw = new FileWriter(new File(fname+num), StandardCharsets.UTF_8);
            BufferedWriter writer = new BufferedWriter(fw);
            writer.append("digraph G {\n\n");
            writer.append(dot(korzeń));
            writer.append("label = \"węzły:"+liczbawezlow+"\";\n}");
            writer.flush();
            writer.close();

            wydajKomendeCmd("python src/pyton/gviz.py "+fname+num);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    static String dot(Węzeł w)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(w.dotRepr());
        if(w.dzieci!=null)
        {
            for (var dz:w.dzieci) {
                sb.append(dot(dz));
            }
        }
        liczbawezlow++;
        return sb.toString();
    }

    public static void wydajKomendeCmd(String komenda) throws IOException {
        ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/C",komenda);
        builder.redirectErrorStream(true);
        Process p = builder.start();
        BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String linia;
        while (true) {
            linia = r.readLine();
            if (linia == null) { break; }
            System.out.println(linia);
        }
    }
}

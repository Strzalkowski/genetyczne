package ms.drzewo.operacje;

import ms.drzewo.Węzeł;
import ms.parser.wyrBaseListener;
import ms.parser.wyrBaseVisitor;
import ms.parser.wyrListener;
import ms.parser.wyrParser;
import org.antlr.v4.runtime.ParserRuleContext;

public class KonstrukcjaDrzewa extends wyrBaseVisitor<Węzeł> {


    @Override
    public Węzeł visitProgram(wyrParser.ProgramContext ctx) {
        return (Węzeł) visit(ctx.wyrazenie());
    }
    @Override
    public Węzeł visitWyrazenieNawiasy(wyrParser.WyrazenieNawiasyContext ctx) {
        return (Węzeł)visit(ctx.wyrazenie());
    }

    @Override
    public Węzeł visitWyrazenieZnak(wyrParser.WyrazenieZnakContext ctx) {
        Węzeł w = visit(ctx.wyrazenie());
        //Jesli trafimy na minusa, mnożymy podrzewo przez -1.
        if(ctx.znak.getText().equals("-"))
        {
            Węzeł r = new Węzeł(Węzeł.Zawartość.RAZY);
            Węzeł z = new Węzeł(null,0,-1);
            r.dzieci.add(z);
            r.dzieci.add(w);
            return r;
        }
        else{return w;}
    }

    @Override
    public Węzeł visitWyrazenieWywolanie(wyrParser.WyrazenieWywolanieContext ctx) {
        Węzeł wz = new Węzeł(ctx.wywolanie_funkcji().NAZWA().getText());
        for (var dz:ctx.wywolanie_funkcji().lista_parametrow_aktualnych().wyrazenie()) {
            Węzeł dzw = (Węzeł) visit(dz);
            dzw.rodzic = wz;
            wz.dzieci.add(dzw);
        }
        return wz;
    }

    @Override
    public Węzeł visitWyrazenieAddyt(wyrParser.WyrazenieAddytContext ctx) {

        Węzeł wz = new Węzeł(Węzeł.Zawartość.zNapisu(ctx.addyt.getText()));
        for (var dz:ctx.wyrazenie()) {
            Węzeł dzw = (Węzeł) visit(dz);
            dzw.rodzic = wz;
            wz.dzieci.add(dzw);
        }
        return wz;
    }

    @Override
    public Węzeł visitWyrazenieMult(wyrParser.WyrazenieMultContext ctx) {
        Węzeł wz = new Węzeł(Węzeł.Zawartość.zNapisu(ctx.mult.getText()));
        for (var dz:ctx.wyrazenie()) {
            Węzeł dzw = visit(dz);
            dzw.rodzic = wz;
            wz.dzieci.add(dzw);
        }
        return wz;
    }

    @Override
    public Węzeł visitWyrazenieStala(wyrParser.WyrazenieStalaContext ctx) {
        return new Węzeł(null,0,Double.parseDouble(ctx.stala_atom().STALA().getText())).loc(ctx.start,ctx.stop);
    }

    @Override
    public Węzeł visitWyrazenieZmienna(wyrParser.WyrazenieZmiennaContext ctx) {
        return new Węzeł(ctx.zmienna().ZMIENNA().getText(),1,1.0).loc(ctx.start,ctx.stop);
    }
}

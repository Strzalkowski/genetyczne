package ms;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.function.Consumer;

public class TestLogicznych {
    public static void main(String[] argv) throws FileNotFoundException {
        String rules_text = new Scanner(new File(argv[0])).useDelimiter("\\Z").next();
        System.out.println(rules_text);
        var rules = new AlarmRules();
        rules.setRules(rules_text);
        //rules.eng.pokażDrzewo();
        //check(rules,4,2,2,2,2,2,null);
        var hits = new HashMap<Integer,Integer>();
        var rulenums = new HashMap<Integer,Integer>();
        checkall((args)->{check(rules,args,hits); rulenums.merge(rules.checkLastRule(), 1, Integer::sum);},null,5,6);
        for (var key:hits.keySet()) {System.out.format("severity %d in %d cases\n",key, hits.get(key));}
        for (var key:rulenums.keySet()) {System.out.format("rule %d hit %d times\n",key, rulenums.get(key));}
    }
    public static void checkall(Consumer<List<Integer>> procedure, List<Integer> args, int maxval, int maxcount)
    {
        if(args==null){args = new ArrayList<>();}
        if(args.size() == maxcount)
        {
            procedure.accept(args);
        }
        if(args.size()<1 || args.size()<maxcount)
        {
            args.add(0);
            for(int i=0;i<=maxval;i++)
            {
                args.set(args.size()-1, i);
                checkall(procedure,args,maxval,maxcount);
            }
            args.remove(args.size()-1);
        }
    }
    public static void check(AlarmRules rules, List<Integer> args, HashMap<Integer,Integer> hits) {check(rules,args.get(0),args.get(1),args.get(2),args.get(3),args.get(4),args.get(5),hits);}
    public static void check(AlarmRules rules, int d,int w, int f, int t, int r, int a, HashMap<Integer,Integer> hits)
    {
        int res = rules.checkAlarm(d,w,f,t,r,a);
        //System.out.format("d%d w%d f%d t%d r%d a%d -> %d @ %d\n",d,w,f,t,r,a,res,rules.checkLastRule());
        if(hits!=null){hits.merge(res, 1, Integer::sum);}
    }
}

package ms.biblioteka;

import ms.drzewo.Drzewo;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class DrzewoAST {
    public wAST korzen;
    public int rozmiar = 1;
    int licznik;

    public void generuj(int gl) {
        korzen = new wAST();
        Queue<wAST> kolejka = new LinkedList<>();
        kolejka.add(korzen);
        Random random = new Random();
        while (!kolejka.isEmpty()) {
            TYP t = wAST.losuj_typ();
            wAST w = kolejka.poll();
            w.typ = t;
            w.dzieci = new wAST[t.n];
            rozmiar += t.n;
            w.wartosc = random.nextDouble() * 200. - 100.;
            for (int i = 0; i < t.n; ++i) {
                wAST tmp = new wAST();
                tmp.rodzic = w;
                tmp.g = w.g + 1;
                if (w.g < gl) {
                    kolejka.add(tmp);
                } else {
                    tmp.typ = wAST.losuj_typ_0();
                    tmp.wartosc = random.nextDouble() * 200. - 100.;
                }
                w.dzieci[i] = tmp;
            }
        }
    }
    public double losuj_wartosc()
    {
        Random random = new Random();
        return random.nextDouble() * 200. - 100.;
    }
    public int postorder(wAST w) {
        if(w == null || w.typ == null)
            return 0;
        int suma = 0;
        for(int i = 0; i < w.typ.n; ++i)
        {
            suma += postorder(w.dzieci[i]);
        }
        w.ld = suma;
        return suma + 1;
    }
    public wAST preorder(wAST w)
    {
        if(licznik == 0)
            return w;
        licznik -= 1;
        for(int i = 0; i < w.typ.n; ++i)
        {
            postorder(w.dzieci[i]);
        }
        return null;
    }

    public wAST losuj_wezel() {
        // nie losuje korzenia
        Random random = new Random();
        licznik = random.nextInt(rozmiar - 1) + 1;
        return preorder(korzen);
    }

    public void mutuj() {
        wAST w = losuj_wezel();
        TYP t = wAST.losuj_typ();
        if(t.n > w.typ.n)
        {
            wAST[] d = w.dzieci;
            w.dzieci = new wAST[t.n];
            w.typ = t;
            for(int i = 0; i < w.typ.n; ++i)
            {
                w.dzieci[i] = d[i];
            }
            for(int i = w.typ.n; i < t.n; ++i)
            {
                wAST nowy = new wAST();
                nowy.typ = wAST.losuj_typ_0();
                nowy.wartosc = losuj_wartosc();
                nowy.rodzic = w;
                nowy.ld = 0;
                nowy.g = w.g +1;
                w.dzieci[i] = nowy;
            }
        }
        else {
            w.typ = t;
            w.wartosc = losuj_wartosc();
        }
    }

    public static void main(String[] args) {
        DrzewoAST drzewo = new DrzewoAST();
        drzewo.generuj(5);
        drzewo.postorder(drzewo.korzen);
        return;
    }
}

package ms.biblioteka;

import java.util.Random;

public class wAST {
    public wAST rodzic;
    public wAST[] dzieci;
    public TYP typ;
    public int g = 1;  // głębokość drzewa
    public int ld = -1; // liczba dzieci w poddrzewach
    public double wartosc = 0;

    static public TYP[] wszystkie = {
            new TYP(TYP.NAZWA_TYPU.stala, 0, 1.0f),
            new TYP(TYP.NAZWA_TYPU.dzialanie, 2, 1.0f),
            new TYP(TYP.NAZWA_TYPU.warunek, 3, 1.0f),
            new TYP(TYP.NAZWA_TYPU.petla, 2, 1.0f),
            new TYP(TYP.NAZWA_TYPU.sekwencja, 2, 1.0f),
            new TYP(TYP.NAZWA_TYPU.wypisanie, 1, 1.0f), // 0 albo 1 dzieci, można użyć pola wartość do wypisanie zmiennej
            new TYP(TYP.NAZWA_TYPU.zapisanie, 1, 1.0f),
    };

    static public TYP losuj_typ()
    {
        Random random = new Random();
        return wszystkie[random.nextInt(wszystkie.length)];
    }
    static public TYP losuj_typ_0()
    {
        return wszystkie[0]; // na razie tylko stała ma 0 dzieci
    }
}

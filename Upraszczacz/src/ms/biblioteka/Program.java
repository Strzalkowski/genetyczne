package ms.biblioteka;

public class Program {
    public DrzewoAST[] osobniki;
    public double[] zmienne = new double[100]; // czy są wypełnione zerami?

    public void krzyzuj(DrzewoAST d1, DrzewoAST d2) {
        wAST w1 = d1.losuj_wezel();
        wAST w2 = d2.losuj_wezel();
        wAST r1 = w1.rodzic;
        wAST r2 = w2.rodzic;
        int i1 = 0;
        int i2 = 0;
        for(; r1.dzieci[i1] != w1; ++i1);
        for(; r2.dzieci[i2] != w2; ++i2);
        int tmpg = w1.g;
        w1.g = w2.g;
        w2.g = tmpg;
        w1.rodzic = r2;
        w2.rodzic = r1;
        r1.dzieci[i1] = w2;
        r2.dzieci[i2] = w1;
    }

    public void test(DrzewoAST d) {

    }

    public void zapisz(String nazwa, DrzewoAST d) {
        // start = korzen
        // kolejka = dzieci start
        // while po kolejce
    }

    public DrzewoAST wczytaj(String nazwa) {
        DrzewoAST drzewo = new DrzewoAST();


        return drzewo;
    }

    public DrzewoAST selekcja(int turniej) {

        return new DrzewoAST();
    }
}

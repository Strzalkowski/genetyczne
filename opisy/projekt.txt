deklaracja_procesu:
(/<instr_procesowa>*\)?wyrażenie(# double* - wartości segmentu anonimowego #)?

przypisanie_procesowe:
nazwa ::= deklaracja_procesu;

instr_procesowa:
przypisanie_procesowe |
use nazwa (,nazwa)*; - import
use-not nazwa (,nazwa)*;- deport
use-clear; | use-purge;
use read[0.12]; - z prawdopodobieśtwem 0.12 (wagą 0.12?)
use max[0.13, 1/R] - na wszystko 0.13, liczba argumantów - 1/R, R - losowa liczba.
instr_silnika: run nazwa;
(/ - zawijasa lewy
inc ::= (/ MEMLIMIT::= 1; use arg; ARGS - NIEMOŻLIWE!
inc musi działać bezpośredno na zmiennej!
inc(12)
package ms.gp.przystosowanie;

import ms.proces.strumienie.StrumieńWyjsciowy;

public interface FitnessFunction{
    void AtExecutionEnd(double[] out);
    void AtExecutionTerminated(double[] out);
    void AtInputStreamFinished(double[] out);
    double compute_fitness(double[] out);
}

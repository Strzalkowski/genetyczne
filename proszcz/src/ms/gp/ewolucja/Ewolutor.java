package ms.gp.ewolucja;

import ms.drzewo.Węzeł;
import ms.gp.populacje.ZarządcaPopulacji;
import ms.gp.przypadki.ZarządcaPrzypadków;
import ms.gp.przystosowanie.FitnessFunction;
import ms.interpreter.Silnik;
import ms.proces.strumienie.IStrumień;
import ms.proces.strumienie.StrumieńWejściowy;
import ms.proces.strumienie.StrumieńWyjsciowy;
import ms.proces.strumienie.StrumieńZTablicą;
import org.antlr.v4.runtime.misc.Pair;

import java.io.PrintStream;
import java.util.ArrayList;

public class Ewolutor {
    PrintStream out = System.out;
    ArrayList<Historia> generacje = new ArrayList<>();
    int numer_generacji_w_iteracji=0;

    double[][] fcases;//dane wejściowe

    int wielkosc_populacji=25000;
    Węzeł[] npop;//nowa populacja
    Węzeł[] spop;//stara populacja
    ArrayList<Pair<Integer,Double>> wyniki;//indeks w populacji - wartości funkcji przystosowania
    Silnik skg;//silnik genetyczny
    boolean interaktywny=false;
    ZarządcaPopulacji zpop = new ZarządcaPopulacji();
    ZarządcaPrzypadków zcas = new ZarządcaPrzypadków();
    FitnessFunction fprzyst;
    void podstawowy_algorytm(int ilosc_generacji, double przyst_stop)
    {
        //1.reprezentacja
        wielkosc_populacji=1000;
        spop = new Węzeł[wielkosc_populacji];
        //inicjalizacja silnika
        skg = new Silnik();
        skg.wykonaj_tekst("use sin,cos;");
        //2.inicjalizacja populacji
        zpop.ramped_and_a_half(spop);
        //Petla - aż warunek zakończenia
        double min_przyst = Double.POSITIVE_INFINITY;
        for(numer_generacji_w_iteracji=0; ;numer_generacji_w_iteracji++)
        {
            //3.Selekcja
            //4.Rekombinacja i mutacja
            następna();
            if(numer_generacji_w_iteracji>=ilosc_generacji)
            {
                out.println("STOPPED, EXCEEDED GENERATION LIMIT:"+ilosc_generacji);
            }
            if(przyst_stop > min_przyst)
            {
                out.println("STOPPED, BEST SPECIMEN ATTAINED FITNESS LOWER THAN TARGET:"+ilosc_generacji);
            }
        }
    }
    void następna()
    {
        //Obliczenie wyników całej populacji
        ewaluuj_populację();
        //3.Selekcja
        //4.Rekombinacja i mutacja
    }
    void ewaluuj_populację()
    {
        if(fcases==null){System.out.println("no fitness cases.Stopped.");return;}
        if(spop==null){System.out.println("no population table.Stopped.");return;}
        for(int fci=0; fci<fcases.length;fci++) {System.out.println("Case"+fci+"empty.Stopped.");return;}
        StrumieńZTablicą we = new StrumieńZTablicą();//
        StrumieńZTablicą wy = new StrumieńZTablicą();//
        skg.główny.ustaw_deskryptor((IStrumień) we,0);//TODO !!!!
        skg.główny.ustaw_deskryptor((IStrumień) wy,1);
        //Dla każdego osobnika
        for(int popi=0; popi<spop.length;popi++)
        {
            skg.główny.drzewo = spop[popi];//podmień drzewo programu głównego w obiekcie silnika.
            //Dla każdego przypadku
            for(int fci=0; fci<fcases.length;fci++)
            {
                we.tab = fcases[fci];//TODO FUNKCJA DO PRZESTAWIANIA
                double wynik = skg.wykonaj(null);//wykonaj główny program.
                //double ocena = fprzyst.compute_fitness(wy.tab);
                //wyniki.add()

            }
        }
    }
    void proc_interaktywna()
    {

    }
}

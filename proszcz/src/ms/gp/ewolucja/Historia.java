package ms.gp.ewolucja;

import ms.drzewo.Węzeł;
import org.antlr.v4.runtime.misc.Pair;

/**
 * Zapisuje, co stało się w danej generacji
 */
public class Historia {
    private int numer;

    double najlepsze_przystosowanie;
    double średnie_przystosowanie;
    double najgorsze_przystosowanie;

    Pair<Węzeł, Double>[] najlepsze_osobniki;
}

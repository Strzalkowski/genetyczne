TinyGP-Java
===========

# Co jest w katalogach:
* import - pełne wyjścia z pewnych uruchomień TinyGp
* zad - dane wejściowe do TinyGp (fitnesscases) i dwa skrypty, z których każdy generuje te pliki (jeden w C++, drugi w Pythonie)
* rozw - zapisane rozwiązania problemów
* src:
    * getresult.py - wyciąga ze standardowego wejścia formułę nastepująca po ostatnim wystąpieniu 'Best Individual' i zapisuje do pliku podanego jako argument
    * start.sh skrót do uruchomienia TinyGp na danym przykładzie i zapisania wyniku w ../rozw przez getresult.py
    * tiny_gp.java
    * uruchamianie.txt - przydatne polecenia
import sys
import re
import pandas as pd
filename = 'out.xlsx'
par=True
header=True
predf = list()
for line in sys.stdin:
    par = not par
    print(line)
    try:#not par or header:
        z = re.match(r"Generation=([0-9]*) Avg Fitness=([^ ]*) Best Fitness=([^ ]*) Avg Size=([^ ]*)\n",line)
        if z:
            header=False
            #print(z.groups())
            predf.append([float(x) if i>0 else int(x) for i,x in enumerate(z.groups())])
        else:
            pass#print(line)
    except KeyboardInterrupt:
        pass#print(line)
#print(predf)
df = pd.DataFrame(predf, columns=['Generation', 'Avg Fitness', 'Best Fitness', ' Avg Size'])
print(df)
df.to_excel("../wyniki/"+filename)
